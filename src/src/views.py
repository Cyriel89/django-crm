from django.http import HttpResponse
from django.shortcuts import redirect, render
from .CRM_utilisateur import Utilisateur

def index(request):

     return render(request, "index.html", context = { "liste_utilisateurs" : Utilisateur.recuperer_utilisateurs()})

def ajouter_utilisateur(request):
     nom = request.POST.get('nom')
     prenom = request.POST.get('prenom')
     age = request.POST.get('age')
     email = request.POST.get('email')
     utilisateur = Utilisateur(nom, prenom, age, email)
     utilisateur.utilisateurToDb()
     return redirect('index')

def supprimer_utilisateur(request):
     nom = request.POST.get('nom')
     prenom = request.POST.get('prenom')
     utilisateur = Utilisateur(nom, prenom)
     utilisateur.supprimer_udb()
     return redirect('index')


