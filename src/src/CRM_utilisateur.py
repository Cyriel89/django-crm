from pathlib import Path
from tinydb import TinyDB, Query, where

class Utilisateur:
    db = TinyDB(Path(__file__).resolve().parent / 'db.json', indent=4)
    utilisateurs = []

    def __init__(self, nom, prenom, age : str = "", email : str = ""):
        self.nom = nom
        self.prenom = prenom
        self.age = age
        self.email = email       

    def recuperer_utilisateurs():
        Utilisateur.utilisateurs.clear()
        r = Utilisateur.db.all()
        for i in r:
           nom = i.get("nom")
           prenom = i.get("prenom")
           age = i.get("age")
           email = i.get("email")
           utilisateur = Utilisateur(nom, prenom, age, email)
           Utilisateur.utilisateurs.append(utilisateur)
        return Utilisateur.utilisateurs
    
    def utilisateurToDb(self):
        if not self.utilisateur_existe():
            Utilisateur.db.insert({"nom" : self.nom, "prenom" : self.prenom, "age" : self.age, "email" : self.email})

    def utilisateur_existe(self):
        return bool(Utilisateur.db.search(where('nom') == self.nom and where("prenom") == self.prenom))
            

    def supprimer_udb(self):
        if self.utilisateur_existe():
            return Utilisateur.db.remove(where('nom') == self.nom and where("prenom") == self.prenom)
